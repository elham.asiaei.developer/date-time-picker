# date-time-picker

> Date Time Picker

[![NPM](https://img.shields.io/npm/v/date-time-picker.svg)](https://www.npmjs.com/package/date-time-picker) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save date-time-picker
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'date-time-picker'
import 'date-time-picker/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [Elham Asiaei](https://github.com/Elham Asiaei)
